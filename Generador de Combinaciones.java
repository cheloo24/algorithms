/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package combinaciones;
import combinaciones.Lector;

/**
 *
 * @author Latorre
 */

//Genera combinaciones de n,r y muestra el progreso detalladamente

//Para compilar:
//javac combina.java
//Para ejecutar:
//java combina <n> <r> [-d]

public class Combina
{
	public static double p = 0, p1 = 0;
        //Boolean para elegir si se procede detallando los resultados
	public static boolean detallado = false;

        //Metodo que hace las combinaciones
	public static void combinaciones(int n, int r)
	{
            //Genera las combinaciones utilizando el algoritmo de Kenneth H. Rosen
            int i = 0, j = 0, ci = 0;
            int v[] = new int[n];

            for(i = 0; i < n; i++)
            {
                v[i]=i;
            } //Inicia v[]

            escribe(v, r);
            ci++;
            while(ci < p1)
            {
                i = r - 1;
                while (v[i] == n - r + i)
                {
                    i--;
                }
                    
                v[i] = v[i] + 1;
                for (j = i + 1; j < r; j++)
                {
                    v[j] = v[i] + j - i;
                }
                escribe(v,r);
                ci++;
            }
	}

        //Metodo para imprimir en consola los resultados
	public static void escribe(int v[], int r)
	{
            if(detallado)
            {
                p++;
                System.out.format("%6.0f) Pasos Totales = %2.0f => %6.2f%% -->", p, p1, p/p1*100.0);
            }
            for(int i = 0; i < r; i++)
            {
                System.out.format("%3d", v[i]);
            }
            System.out.println("");
	}
	
        //Funcion que calcula el factorial
	public static double factorial(int n, int m)
	{
            //Multiplica desde n hasta m-1. n>=m
            double f = 1;
            while(n > m)
            {
                f = f * n;
		n--;
            }
            return f;
	}
        
        //Metodo Principal
	public static void main(String args[])
	{
            //Declaracion de Variables
            int n = 0, r = 0;
            String detallar = "";
            
            //Creacion de la instancia del Lector por consola:
            //http://gl-epn-programacion-ii.blogspot.com/2010/11/lectura-de-datos-por-consola.html
            Lector lector = new Lector();
            
            //Imprime cabecera del programa y solicita ingreso de datos
            System.out.println("Programa para Generar combinaciones de n, r");
            
            try
            {
                System.out.println("Ingresa el Valor de n: ");
                n = Integer.parseInt(lector.leer());
                System.out.println("Ingresa el Valor de r: ");
                r = Integer.parseInt(lector.leer());
                System.out.println("Deseas ver el Progreso de las Operaciones (s/n): ");
                detallar = lector.leer();
            }
            catch(Exception ex)
            {
                System.out.println("Error - Verifica los datos ingresados.");
                System.out.println("[+] n,r deben ser enteros mayores que 0");
		System.out.println("[+] n >= r");
		System.out.println("[+] s/n muestra el progreso de la operacion");
            }
            
            if (detallar.toLowerCase().equals("s"))
            {
                detallado = true;
            }

            if (n<1 || r<1 || n<r)
            {
                System.out.println("Verifica los valores de n y r.");
            }
            
            //p1=factorial(n)/factorial(r)/factorial(n-r)
            //Se implementa de esta forma para evitar errores de redondeo
            if((n-r)>r)
            {
                p1=factorial(n,n-r)/factorial(r,1);
            }
            else
            {
                p1=factorial(n,r)/factorial(n-r,1);
            }
            combinaciones(n,r);
        }
}
