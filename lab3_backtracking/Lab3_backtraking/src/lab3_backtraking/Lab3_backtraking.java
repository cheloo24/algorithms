/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3_backtraking;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Camilon
 */


public class Lab3_backtraking {

   public static void imprimirSolucion(char mazmorraFinal[][],int tamaño)
{
    for (int i = 0; i < tamaño; i++)
    {
        for (int j = 0; j < tamaño; j++)
            System.out.print(mazmorraFinal[i][j]);
        System.out.println(" ");
        
    }
     File archivo = new File("salida.out.txt");//se crea el archivo
        BufferedWriter bw;//para escribir
        try{
        if(archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write("El fichero de texto ya estaba creado.");
        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
            
        
  
    for (int i = 0; i < tamaño; i++)
    {
        for (int j = 0; j < tamaño; j++)
        {    bw.write(mazmorraFinal[i][j]);
        
        }bw.newLine();
    }
         
        }
        bw.close();   } catch (IOException | NumberFormatException e){ //Catch de excepciones
            
            System.err.println("Ocurrio un error: " + e.getMessage());
        }
}
 
public static boolean esCamino(char mazmorra[][], int a, int b,int tamaño)  //revisa si la posicion x,y en la matriz es un camino por donde se puede pasar
{
   
    if(a >= 0 && a < tamaño && b >= 0 && b < tamaño && mazmorra[a][b] =='1')
    {   return true;}
 
    return false;
}
 
public static boolean esCaminoIzq(char mazmorra[][], int a, int b,int tamaño)  //revisa si la posicion x,y en la matriz es un camino por donde se puede pasar
{
   
    if(a >= 0 && a < tamaño && b >= 1 && b < tamaño && mazmorra[a][b] =='1')
    {   return true;}
 
    return false;
}
 
/* funcion recursiva pra buscar caminos */
public static boolean BuscarCamino(char mazmorra[][], int a, int b, char mazmorrafinal[][],int tamaño,int mov)
{
    // caso base, si se llegó a la posición destino se marca esta posicion y se termina el programa
    if(a == tamaño-1 && b == tamaño-1)
    {
        mazmorrafinal[a][b] = 'X';
        return true;
    }
 
    //revisa con la funcion es camino si la posicion x,y es valida
    if(esCamino(mazmorra, a, b,tamaño) == true)
    {
        // marcamos el camino que vamos 
        mazmorrafinal[a][b] = 'X';
      /* nos movemos hacia abajo si es que el ultimo movimiento no fue hacia abajo */
if(mov!=3){
        if (BuscarCamino(mazmorra, a+1, b, mazmorrafinal,tamaño,0) == true)
        {   
            return true;}  }
 
        /* si no hay camino abajo nos movemos hacia derecha, si es que el ultimo movimiento fue hacia abajo o derecha o arriba */
        if(mov==1||mov==0||mov==3){
        if (BuscarCamino(mazmorra, a, b+1, mazmorrafinal,tamaño,1) == true)
        {
            return true;}}  
        //nos movemos hacia izquierda, si es que el ultimo movimiento fue izq, o abajo o arriba
        if(b>=1&&mov==0||b>=1&&mov==2||b>=1&&mov==3){
        if (BuscarCamino(mazmorra, a, b-1, mazmorrafinal,tamaño,2) == true)
        {//System.out.println("entre con"+ultimomovimiento);
            
            return true;} } 
        //nos movemos hacia arriba, si es que el ultimo movimiento fue izq, der o arriba
        if(a>=1&&mov==2||a>=1&&mov==1||a>=1&&mov==3){
        if (BuscarCamino(mazmorra, a-1, b, mazmorrafinal,tamaño,3) == true)
        {return true;} } 
        /* si no funcionan los anteriores se hace backtracking y se desmarca los valores incorrectamente marcados*/
        mazmorrafinal[a][b] = '1';
        return false;
    }  
 
    return false;
}
    public static boolean llenador(){ 
       int tamaño=1;
       char mazmorra[][];
       char mazmorraFinal[][];
        try{
            
            // Abrimos el archivo
            FileInputStream fstream = new FileInputStream("entrada.txt");
            // Creamos el objeto de entrada
            DataInputStream entrada = new DataInputStream(fstream);
            // Creamos el Buffer de Lectura
            BufferedReader buffer = new BufferedReader(new InputStreamReader(entrada));
            String strLinea;
            // Leer el archivo linea por linea
            strLinea = buffer.readLine();
            tamaño=Integer.parseInt(strLinea);
            mazmorra=new char[tamaño][tamaño];
            mazmorraFinal=new char[tamaño][tamaño];
            int i=0;
            while ((strLinea = buffer.readLine()) != null){
                char[] aCaracteres;
            aCaracteres = strLinea.toCharArray();
            for(int j=0;j<tamaño;j++){ 
            //    System.out.print(aCaracteres[j]);
            mazmorra[i][j]=aCaracteres[j];
            mazmorraFinal[i][j]=aCaracteres[j];
            
            }
            i++;
            }
          
            entrada.close();
            
 
    if(BuscarCamino(mazmorra, 0, 0, mazmorraFinal,tamaño,0) == false)
    {
        System.out.print("no hay solución");
        return false;
    }
 
    imprimirSolucion(mazmorraFinal,tamaño);
    return true;
    
        } 
        catch (IOException | NumberFormatException e){ //Catch de excepciones
            mazmorra=new char[1][1];
            mazmorra[0][0]='0';
            
            System.err.println("Ocurrio un error: " + e.getMessage());
            return false;
        }
   
    }  
    public static void main(String[] args) {
        
     
        llenador();
      
    }
    
}