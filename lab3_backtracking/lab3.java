﻿package backtracking;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Chelo
 */


public class Backtracking {

   public static void imprimirSolucion(char mazmorraFinal[][],int tamaño)
{
    for (int i = 0; i < tamaño; i++)
    {
        for (int j = 0; j < tamaño; j++)
            System.out.print(mazmorraFinal[i][j]);
        System.out.println(" ");
        
    }
     File archivo = new File("salida.out.txt");//se crea el archivo
        BufferedWriter bw;//para escribir
        try{
        if(archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write("El fichero de texto ya estaba creado.");
        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
            
        
  
    for (int i = 0; i < tamaño; i++)
    {
        for (int j = 0; j < tamaño; j++)
        {    bw.write(mazmorraFinal[i][j]);
        
        }bw.newLine();
    }
         
        }
        bw.close();   } catch (IOException | NumberFormatException e){ //Catch de excepciones
            
            System.err.println("Ocurrio un error: " + e.getMessage());
        }
}
 
public static boolean esCamino(char mazmorra[][], int x, int y,int tamaño) 
{
   
    if(x >= 0 && x < tamaño && y >= 0 && y < tamaño && mazmorra[x][y] =='1')
    {   return true;}
 
    return false;
}
 

 

public static boolean BuscarCamino(char mazmorra[][], int x, int y, char mazmorrafinal[][],int tamaño)
{
    
    if(x == tamaño-1 && y == tamaño-1)
    {
        mazmorrafinal[x][y] = 'X';
        return true;
    }
 
  
    if(esCamino(mazmorra, x, y,tamaño) == true)
    {
       
        mazmorrafinal[x][y] = 'X';
 
 
        if (BuscarCamino(mazmorra, x+1, y, mazmorrafinal,tamaño) == true)
        {return true;}  
 
        if (BuscarCamino(mazmorra, x, y+1, mazmorrafinal,tamaño) == true)
        {return true;}  
       

        mazmorrafinal[x][y] = '0';
        return false;
    }  
 
    return false;
}
    public static boolean llenador(){ 
       int tamaño=1;
       char mazmorra[][];
       char mazmorraFinal[][];
        try{
            
            // Abrimos el archivo
            FileInputStream fstream = new FileInputStream("entrada.txt");
            // Creamos el objeto de entrada
            DataInputStream entrada = new DataInputStream(fstream);
            // Creamos el Buffer de Lectura
            BufferedReader buffer = new BufferedReader(new InputStreamReader(entrada));
            String strLinea;
            // Leer el archivo linea por linea
            strLinea = buffer.readLine();
            tamaño=Integer.parseInt(strLinea);
            mazmorra=new char[tamaño][tamaño];
            mazmorraFinal=new char[tamaño][tamaño];
            int i=0;
            while ((strLinea = buffer.readLine()) != null){
            
                char[] aCaracteres;
            aCaracteres = strLinea.toCharArray();
            for(int j=0;j<tamaño;j++){ 
            //    System.out.print(aCaracteres[j]);
            mazmorra[i][j]=aCaracteres[j];
            mazmorraFinal[i][j]=aCaracteres[j];
            
            }i++;
            }
          
            entrada.close();
            
 
    if(BuscarCamino(mazmorra, 0, 0, mazmorraFinal,tamaño) == false)
    {
        System.out.print("no hay solución");
        return false;
    }
 
    imprimirSolucion(mazmorraFinal,tamaño);
    return true;
    
        } 
        catch (IOException | NumberFormatException e){ //Catch de excepciones
            mazmorra=new char[1][1];
            mazmorra[0][0]='0';
            
            System.err.println("Ocurrio un error: " + e.getMessage());
            return false;
        }
   
    }  
    public static void main(String[] args) {
        
        char matriz[][];
        llenador();
       // for(int i=0;i<4;i++){
         //   for(int j=0;i<4;i++){
           //     System.out.print(matriz[i][j]);
            //}
            //System.out.println(" ");
        //}
    }
    
}
