#include <stdio.h>
#include <stdlib.h>
#define MAX 49
#define valorloto 10

void printc(int comb[], int k) {
    FILE *Fichero2;
    Fichero2 = fopen("salida.out","w");
    if(k>6 && k <=valorloto){
        fputs("k = ",Fichero2);
        char *p = malloc(128*sizeof(char));
        sprintf(p,"%d",k);
        fputs(p,Fichero2);
        fputs("\n",Fichero2);
        fputs("{",Fichero2);
        int i;
        for (i = 0; i < k; ++i){
            int a = comb[i] + 1;
            char *c = malloc(128*sizeof(char));
            sprintf(c,"%d",a);
            fputs(c,Fichero2);
            fputs(", ",Fichero2);
            printf("%s, ",c);
        }
            //printf("%d, ", comb[i] + 1);
        fputs("\b\b}\n",Fichero2);
    }
    else{
        fputs("k = ",Fichero2);
        char *p = malloc(128*sizeof(char));
        sprintf(p,"%d",k);
        fputs(p,Fichero2);
        fputs("\n",Fichero2);
    }
    //printf("\b\b}\n");
    fclose(Fichero2);
}

int next_comb(int comb[], int k, int n) {
    int i = k - 1;
    ++comb[i];
    while ((i >= 0) && (comb[i] >= n - k + 1 + i)) {
        --i;
        ++comb[i];
    }

    if (comb[0] > n - k)
        return 0;

    for (i = i + 1; i < k; ++i)
        comb[i] = comb[i - 1] + 1;

    return 1;
}

void lectura_archivo(){
    FILE *Fichero1;
    Fichero1 = fopen("entrada.in","r");
    char *linea = (char *)malloc(512*sizeof(char));
    char * aux = (char*)malloc(128*sizeof(char));

    while(fgets(linea,512,Fichero1)!= NULL){
        aux = strtok(linea,"\n");
        int comb[MAX]; //tama�o del set
        int i;
        for (i = 0; i < atoi(aux); ++i)
            comb[i] = i;
        printc(comb, atoi(aux));
        while (next_comb(comb, atoi(aux), valorloto)){
            printc(comb, atoi(aux));
        }
        while((aux = strtok(NULL," "))!= NULL){
            int comb[MAX]; //tama�o del set
            int i;
            for (i = 0; i < atoi(aux); ++i)
                comb[i] = i;
            printc(comb, atoi(aux));
            while (next_comb(comb, atoi(aux), valorloto))
                printc(comb, atoi(aux));

        }

    }

    printf("Operacion realizada con exito, busque el archivo salida.out para ver resultado");
    fclose(Fichero1);
}
int main(int argc, char *argv[]) {

    lectura_archivo();

    /*const int y = 6;
    int k = 10; // en grupos de k
    int comb[MAX]; //tama�o del set
    int i;
    for (i = 0; i < k; ++i)
        comb[i] = i;
    printc(comb, k);
    while (next_comb(comb, k, valorloto))
        printc(comb, k);
*/

    return 0;
}
