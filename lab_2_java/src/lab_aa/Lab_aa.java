
package lab_aa;
import java.io.*;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Lab_aa {

public static int n; //numero de filas
public static int m; //numero de columnas
public static char matriz[][]; //matriz que sera llenada con el color de los lienzos

static void CalculaFilas() throws IOException{ //Esta función solo asigna los valores de n y m
    FileReader fr = null;
    try {
        File archivo = new File("entrada.in");
        fr = new FileReader(archivo);
        BufferedReader br = new BufferedReader(fr);
        String linea;
        while((linea = br.readLine()) != null){
            StringTokenizer st = new StringTokenizer(linea); //inicio el token
            while(st.hasMoreTokens()){ // asigno el valor tokenizado por un espacio a la variable m
                n = Integer.parseInt(st.nextToken()); // asigno el valor tokenizado por un espacio a la variable n
                m = Integer.parseInt(st.nextToken()); // asigno el valor tokenizado por un espacio a la variable m
            }
        }
        matriz = new char[n][m];
        fr.close(); //cierro la lectura del archivo
    } catch (FileNotFoundException ex) {
        Logger.getLogger(Lab_aa.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
        try {
            fr.close();
        } catch (IOException ex) {
            Logger.getLogger(Lab_aa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

static char EntregaColorAzar(){ // funcion que entrega un color al azar
    int aux = (int) (Math.random()*4+1); //calcula un numero al azar entre 1 y 4
    char color;
    switch(aux){
        case 1: color = 'A';    //si el nro al azar es 1 sera color azul
                break;   
        case 2: color = 'R';    //si el nro al azar es 2 sera color rojo
                break;
        case 3: color = 'V';    //si el nro al azar es 3 sera color verde
                break;
        case 4: color = 'N';    //si el nro al azar es 4 sera color naranja
                break;
        default: color = '1';   //si el nro es cualquier otro, no será nigun color
                break;
    }
    return color;   
}

static void LlenaMatriz(char matriz [][]){
    matriz[0][0] = EntregaColorAzar();
    
    /* Primero se llena con una letra al azar
     * la primera posicion del arreglo
     */
  
    for(int k=1;k<m;k++){
        char aux = EntregaColorAzar();
        while(matriz[0][k-1] == aux)    //se verifica que no se repita el caracter con el adyacente.
            aux = EntregaColorAzar();
        matriz[0][k] =aux;
    }
    
    /*
     * Luego se llena la primera fila de la matriz
     */
    
    for(int i =1;i<n;i++){
        char temp = EntregaColorAzar();
        for(int j =0;j<m;j++){
            if(j==0){
                while(matriz[i-1][j] == temp)
                    temp = EntregaColorAzar();
                matriz[i][j] = temp;
            }
            else{
                while(matriz[i-1][j] == temp && matriz[i][j-1]== temp
                        || matriz[i-1][j] != temp && matriz[i][j-1]== temp
                        ||matriz[i-1][j] == temp && matriz[i][j-1]!= temp)
                    temp = EntregaColorAzar();
                matriz[i][j] = temp;
            }         
        }
    }
    /*
     * Finalmente se llena la matriz mediante un patrón que verifica
     * la adyacencia vertical y horizontal
     * teniendo en cuenta los casos en que se esta en la primera columna
     * de la matriz, donde no verifica adyacencia horizontal por la izq.
     */
}

static void ImprimeMatriz(char matriz[][]) throws IOException{ //Imprime la matriz
    FileWriter archivo = new FileWriter("salida.out");
    PrintWriter pw = new PrintWriter(archivo);
    for(int i =0;i<n;i++){
        pw.print("|");
        for(int j=0;j<m;j++){
            pw.print(matriz[i][j]+"|");
            if(j == m-1)
                pw.println("");
            
        }
    }
    int contA = 0;
    int contR = 0;
    int contV = 0;
    int contN = 0;
    for(int i =0;i<n;i++){
        for(int j =0;j<m;j++){
            if(matriz[i][j] == 'A')
                contA +=1;
            if(matriz[i][j] == 'N')
                contN +=1;
            if(matriz[i][j] == 'R')
                contR +=1;
            if(matriz[i][j] == 'V')
                contV +=1;
            
        }
    }
    pw.print("Total de casillas: "+contR+" rojas, "+contA+" amarillas, "+contV+" verdes y "+contN+" naranjas.");
    archivo.close();
}


    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        CalculaFilas();
        LlenaMatriz(matriz);
        ImprimeMatriz(matriz);
        
    }
}
